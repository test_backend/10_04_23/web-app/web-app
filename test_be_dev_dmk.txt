1. WEB_APP

- Buatlah sebuah aplikasi web sederhana dengan bahasa PHP (Laravel), dengan ketentuan sebagai berikut:

a. Tema aplikasi tentang Document management.
b. Fitur utama :
	- Kelola petugas (CRUD Petugas).
	- Kelola kategori surat (CRUD Category)
	- Kelola Surat (CRU-Disable Surat)
c. Terdapat 2 jenis user/pengguna (Admin & petugas).
   * Fitur Admin 
	- Kelola petugas
	- Kelola kategori surat
	- Kelola surat (dapat menampilkan seluruh surat)
   
   * Fitur Petugas
	- Kelola Surat (hanya menampilkan surat yang dibuat oleh petugas yang bersangkutan).

d. Field utama user = id, nama, username, password, role_id
e. Field utama kategory = id, kode kategori, nama kategori.
f. Field utama surat = id, kode_surat, kategori surat, tanggal_pembuatan, id_user/username, title, body, image.
g. Format kode surat = urutan-kode_kategori-date. ex : 001-SI-10042023
 * Untuk kode surat apabila ganti bulan maka urutan surat akan dihitung dari awal (reset urutan).
h. Untuk tampilan peserta bebas menggunakan template (bootstrap) selama pembuatan aplikasi.

2. WEB_API

Ketentuan sama seperti nomor 1 point A-G
Silahkan gunakan laravel sanctum sebagai authentikasi API.
Gunakan aplikasi postman untuk testing API yang telah dibuat


